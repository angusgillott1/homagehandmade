#include "tests.h"

int32 xOffset = 0;
int32 yOffset = 0;
Bitmap* debugBitmap = 0;

void setupForTesting()
{
  debugBitmap = (retrieveSpritesheet("RIN_SPRITESHEET"))->allFrames[0][0];

  //debugBitmap = generateAsciiImage(retrieveBitmap("AIRPORT_BITMAP"));
}

void runInitialTestSuite()
{
  testVarDump();
  testSound();
}

void testVarDump()
{ 
  dumpMessage("Testing varDump()...");
  varDump("TestString");
  varDump(589);
  varDump((float)589.79323846);
  varDump(0.0);
  varDump((float)0.335);

  dumpMessage("Testing quickHash()...");
  varDump("AA");
  varDump(quickHash("AA"));
  varDump("AB");
  varDump(quickHash("AB"));
  varDump("AC");
  varDump(quickHash("AC"));
  varDump("BA");
  varDump(quickHash("BA"));
  varDump("BB");
  varDump(quickHash("BB"));
  varDump("BC");
  varDump(quickHash("BC"));
  varDump("CC");
  varDump(quickHash("CC"));
  varDump("AZ");
  varDump(quickHash("AZ"));
  varDump("ZA");
  varDump(quickHash("ZA"));
  varDump("ZZ");
  varDump(quickHash("ZZ"));
  varDump("YZ");
  varDump(quickHash("YZ"));
  varDump("_Z");
  varDump(quickHash("_Z"));
  varDump("A");
  varDump(quickHash("A"));
  varDump("X");
  varDump(quickHash("X"));

  dumpMessage("Testing isStringMatch()");
  varDump("Quick Slick");
  varDump(isStringMatch("Quick", "Slick"));
  varDump("Quick Quick");
  varDump(isStringMatch("Quick", "Quick"));
  varDump("Qui Quick");
  varDump(isStringMatch("Qui", "Quick"));
  varDump("Quick Qui");
  varDump(isStringMatch("Quick", "Qui"));
  varDump("Slick Slick");
  varDump(isStringMatch("Slick", "Slick"));
  varDump("'' ''");
  varDump(isStringMatch("", ""));
  varDump("'' Quick");
  varDump(isStringMatch("", "Quick"));
  varDump("Quick ''");
  varDump(isStringMatch("Quick", ""));
}

void testSound()
{
  playMusic(retrieveMusic("BIT_QUEST"));
  playSound(retrieveSound("WOLF_GROWL"));
}

void testRendering()
{
  /*for(uint32 i = 0; i < 10000; i++)
  {
    uint32 sx = getWindowWidth() - 30; //rand() % (getWindowWidth() - 1 - 32);
    uint32 sy = rand() % (getWindowHeight() - 1 - 32);

    renderBitmap(debugBitmap, sx, sy);
  }*/

  //drawTextAtAbsolutePosition("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_ `abcdefghijklmnopqrstuvwxyz{|}~N", 50, 700);

  //drawAsciiImage(debugAsciiImage);

  //debugBitmap = retrieveBitmap("COFFEE_BITMAP");
  
  //renderBitmap(retrieveBitmap("AIRPORT_BITMAP"),0,0);
  
  renderBitmap(debugBitmap, xOffset, yOffset);

  /*ColorRGBA red;
  red.r = 255;
  red.g = 0;
  red.b = 0;
  red.a = 255;
  setPixel(959, 765, &red);*/

  /*renderBitmap(debugBitmap, xOffset, yOffset);
  ColorRGBA purple;
  purple.r = 75;
  purple.g = 0;
  purple.b = 137;
  purple.a = 255;
  drawStraightLine(xOffset, yOffset, 32, &purple);
  drawStraightLine(xOffset, yOffset, 32, &purple, TRUE);
  drawStraightLine(xOffset, yOffset + 31, 32, &purple);
  drawStraightLine(xOffset + 31, yOffset, 32, &purple, TRUE);

  ColorRGBA yellow;
  yellow.r = 255;
  yellow.g = 255;
  yellow.b = 0;
  yellow.a = 255;
  drawRectangle(xOffset + 64, yOffset + 64, 64, 64, &yellow);

  ColorRGBA blue;
  blue.r = 0;
  blue.g = 0;
  blue.b = 255;
  blue.a = 255;
  drawRectangle(xOffset + 128, yOffset + 128, 16, 16, &blue, TRUE);*/
}

void handleDebugInput()
{
  boolean_int* keyDown = getKeyDownArray();
  boolean_int* keyWasDown = getKeyWasDownArray();

  if(keyDown[VK_UP])
    yOffset--;
  if (keyDown[VK_DOWN])
    yOffset++;
  if (keyDown[VK_LEFT])
    xOffset--;
  if (keyDown[VK_RIGHT])
    xOffset++;

  if(keyDown['S'])
  {    
    debugBitmap = (retrieveSpritesheet("RIN_SPRITESHEET"))->allFrames[rand() % 4][rand() % 3];

    if(!keyWasDown['S'])
    {
      playSound(retrieveSound("WOLF_GROWL"));
    }
  }

  if(keyDown['F'])
  {
    debugBitmap = (retrieveFontSet("MONOGRAM_FONT_SMALL_RED"))->characters[rand() % 128];
  }
}