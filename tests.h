#pragma once

#include <windows.h>

#include "input.h"
#include "utils.h"
#include "audio.h"
#include "assets.h"
#include "render.h"

void setupForTesting();
void runInitialTestSuite();

void testVarDump();
void testSound();
void testRendering();
void handleDebugInput();