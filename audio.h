#pragma once

#include <windows.h>

#include <SDL.h>
#include <SDL_mixer.h>

#include "typedefs.h"
#include "utils.h"

void preloadSound(const char* filepath, const char* name);
Mix_Chunk* retrieveSound(const char* name);
void playSound(Mix_Chunk* sound);

void preloadMusic(const char* filepath, const char* name);
Mix_Music* retrieveMusic(const char* name);
void playMusic(Mix_Music* music);

void preloadSoundAssets();

void setupAudio();