#define _CRT_SECURE_NO_WARNINGS

#include "utils.h"

//--------------------
// Strings
//--------------------

void fillString(char* strTo, char c, int32 nCharacters)
{
  memset(strTo, c, nCharacters);
  strTo[nCharacters - 1] = '\0';
}

int32 rawStringCopy(char* strTo, const char* strFrom, int32 strToIndex = 0, int32 strFromIndex = 0, int32 maxCharsToCopy = MAX_POSSIBLE_INT_32)
{
  if(strToIndex < 0)
  {
    return strToIndex;
  }
  
  int32 i = strToIndex;
  int32 j = strFromIndex;
  int32 n = 0;

  while(strTo[i] != '\0' && strFrom[j] != '\0' && n < maxCharsToCopy)
  {
    strTo[i] = strFrom[j];
    i++;
    j++;
    n++;
  }

  if(strTo[i] == '\0')
  {
    return -1;
  }
  else
  {
    return i;
  }
}

boolean_int isStringMatch(const char* str1, const char* str2)
{
  char* str1p = (char*)str1;
  char* str2p = (char*)str2;
  
  while(*str1 != '\0' && *str2 != '\0') // Until one string runs out
  {
    if(*str1 != *str2)  // This means the two strings had a different character at the same index; they are not the same 
       return 0;

    str1++;
    str2++;
  }

  if(*str1 != '\0' || *str2 != '\0')  // This means strings were different length; they are not the same.
  {
    return 0;
  }

  return 1;
}

//-----------------------
// Debug output
//-----------------------

void varDump(const char* var)
{
  const int32 maxStringLength = 64;
  char str[maxStringLength];
  fillString(str, '_', maxStringLength);

  int32 nextCharIndex = rawStringCopy(str, var, rawStringCopy(str, "String: "));

  if(nextCharIndex != -1)
  {
    str[nextCharIndex] = '\0';
  }

  dumpMessage(str);
}

void varDump(float var)
{
  const int32 maxStringLength = 64;
  char str[maxStringLength];
  fillString(str, '_', maxStringLength);

  char fstr[20];
  char digits[20];
  int32 nDigits = 0;

  int32 intPart = (int)var;
  int32 fiveDecimals = (int)((var - intPart) * 100000);
  boolean_int wasInt = (fiveDecimals > 0);
  boolean_int wasZero = (intPart + fiveDecimals == 0);

  while(fiveDecimals > 0)
  {
    int32 d = fiveDecimals - ((fiveDecimals / 10) * 10);      
    digits[nDigits] = (char)d + '0';
    nDigits++;
    fiveDecimals /= 10;
  }

  if(wasInt)
  {
    digits[nDigits] = '.';
    nDigits++;
  }

  while(intPart > 0)
  {
    int32 d = intPart - ((intPart / 10) * 10);      
    digits[nDigits] = (char)d + '0';
    nDigits++;
    intPart /= 10;
  }

  if(wasZero)
  {
    digits[0] = '0';
    nDigits++;
  }

  for(int32 i = 0; i < nDigits; i++)
  {
    fstr[i] = digits[nDigits - 1 - i];
  }
  fstr[nDigits] = '\0';

  int32 nextCharIndex = rawStringCopy(str, fstr, rawStringCopy(str, "Number: "));

  if(nextCharIndex != -1)
  {
    str[nextCharIndex] = '\0';
  }

  dumpMessage(str);
}

void dumpMessage(const char* msg)
{
  const int32 maxStringLength = 512;
  char str[maxStringLength];
  fillString(str, '_', maxStringLength);
  
  int32 nextCharIndex = rawStringCopy(str, msg);

  if(nextCharIndex == -1)
  {
    str[maxStringLength - 2] = '\n';
    goto STRING_READY;
  }

  str[nextCharIndex] = '\n';
  str[nextCharIndex + 1] = '\0';

STRING_READY: 

  OutputDebugStringA(str);
}

//--------------------------
// Quickmap
//--------------------------

QuickMapNode* createQuickMapNode(void* item, const char* key)
{
  QuickMapNode* newNode = (QuickMapNode*)malloc(sizeof(QuickMapNode));

  newNode->item = item;
  newNode->nextNode = 0;

  memset(newNode->key, ' ', sizeof(newNode->key));
  int nextCharacterIndex = rawStringCopy(newNode->key, key);
  newNode->key[nextCharacterIndex] = '\0';

  return newNode;
}

QuickMap* createQuickMap()
{
  QuickMap* newQuickMap = (QuickMap*)malloc(sizeof(QuickMap));
  memset(newQuickMap, 0, sizeof(QuickMap));

  return newQuickMap;
}

int32 quickHash(const char* str)
{
  char firstChar = str[0];
  char secondChar = str[1];
  int32 firstIndex = (firstChar - 'A') / 2;
  int32 secondIndex = (secondChar - 'A') / 2;
  
  if(firstChar == '_' || secondChar == '\0')  // If string starts with underscore or is only one character long, just hash to first bucket.
  {
    return 0;
  }

  return (13 * firstIndex + secondIndex);
}

void pushToMapBucket(QuickMapNode* firstNode, void* item, const char* key) // firstNode must not be a null pointer.
{
  QuickMapNode* newNode = createQuickMapNode(item, key);

  QuickMapNode* traversingNode = firstNode;
  QuickMapNode* lastNode = firstNode;

  while(traversingNode != 0)
  {
    lastNode = traversingNode;
    traversingNode = traversingNode->nextNode;
  }

  lastNode->nextNode = newNode;
}

void* retrieveItem(QuickMap* quickMap, const char* key)
{
  int32 hashKey = quickHash(key);
  QuickMapNode* traversingNode = quickMap->buckets[hashKey];

  while(traversingNode != 0)
  {
    if(isStringMatch(key, traversingNode->key))
    {
      return traversingNode->item;
    }

    traversingNode = traversingNode->nextNode;
  }

  return 0;
}

void storeItem(QuickMap* quickMap, void* item, const char* key)
{
  int32 hashKey = quickHash(key);

  QuickMapNode* bucketFirstNode = quickMap->buckets[hashKey];

  if(bucketFirstNode == 0)
  {
    QuickMapNode* newNode = createQuickMapNode(item, key);
    quickMap->buckets[hashKey] = newNode;
  }
  else
  {
    pushToMapBucket(bucketFirstNode, item, key);
  }
}