#include "assets.h"

QuickMap* allBitmaps;
QuickMap* allSpritesheets;
QuickMap* allFontSets;

int32 getNumberOfCharactersPerFont()
{
  return NUMBER_OF_CHARACTERS_PER_FONT;
}

void freeBitmap(Bitmap* bitmap)
{
  free(bitmap->transparencyMap);
  free(bitmap->pixelArrayStart);
  free(bitmap);
}

Bitmap* generateSubBitmap(Bitmap* bitmap, uint32 sampleOffsetX, uint32 sampleOffsetY, uint32 sampleWidth, uint32 sampleHeight)
{
  uint32 bitmapWidth = bitmap->width;
  uint32 bitmapHeight = bitmap->height;

  uint32* firstPixelInSource = bitmap->pixelArrayStart + (sampleOffsetX) + (bitmapWidth * sampleOffsetY);
  uint32 subBitmapPixels = sampleWidth * sampleHeight;

  uint32* srcCopyPointer = firstPixelInSource;

  Bitmap* subBitmap = (Bitmap*)malloc(sizeof(Bitmap));

  subBitmap->width = sampleWidth;
  subBitmap->height = sampleHeight;
  subBitmap->pixelArrayStart = (uint32*)malloc(subBitmapPixels * 4);

  uint32* destCopyPointer = subBitmap->pixelArrayStart;

  for(uint32 k = 0; k < sampleHeight; k++)
  {
    memcpy(destCopyPointer, srcCopyPointer, sampleWidth * 4);
    destCopyPointer += sampleWidth;
    srcCopyPointer += bitmapWidth;
  }

  generateBitmapTransparencyMap(subBitmap);

  return subBitmap;
}

void applyColorMaskToBitmap(Bitmap* bitmap, ColorRGBA* maskColor)
{
  uint32* startOfPixels = bitmap->pixelArrayStart;

  uint32 nPixels = bitmap->width * bitmap->height;

  for(uint32 i = 0; i < nPixels; i++) // go through each pixel
  {
    uint32* pixel = bitmap->pixelArrayStart + i;
    byte alpha = (byte)(((255 << 24) & *pixel) >> 24);

    if(alpha)
    {
      *pixel = ((alpha << 24) | (maskColor->r << 16) | (maskColor->g << 8) | maskColor->b);
    }
  }

  return;
}

void generateBitmapTransparencyMap(Bitmap* bitmap)
{
  uint32 nPixels = bitmap->width * bitmap->height;

  uint32* transparencyMapStart = (uint32*)malloc(nPixels * 4);

  uint32 n = 0; // the current counter of pixels in this run
  uint32 k = 0;  // the index in the transparency map array
  for(uint32 i = 0; i < nPixels; i++) // go through each pixel
  {
    boolean_int currentRunIsTransparent = (k % 2) == 0;
    uint32 pixel = *(bitmap->pixelArrayStart + i);
    byte alpha = (byte)(((255 << 24) & pixel) >> 24);

    if((currentRunIsTransparent && !alpha) || (!currentRunIsTransparent && alpha))
    {
      n++;
      continue; // we are still in the current run
    }
    else
    {
      *(transparencyMapStart + k) = n;
      n = 1;
      k++;
    }
  }
  *(transparencyMapStart + k) = n;  // store the last pixel
  k++;
  while (k < nPixels)
  {
    *(transparencyMapStart + k) = 0; // the run has ended; (logically we have 0 transparent pixels followed by 0 opaque pixels followed by 0 transparent pixels)
    k++;
  }

  bitmap->transparencyMap = transparencyMapStart;
}

void preloadBitmap(const char* filepath, const char* name)
{
  Bitmap* newBitmap = (Bitmap*)malloc(sizeof(Bitmap));

  uint32 fileSize = getFileSize(filepath);
  void* wholeFileMemoryAddressStart = malloc(fileSize);
  loadEntireFileIntoMemory(filepath, fileSize, wholeFileMemoryAddressStart);

  byte* b = (byte*)wholeFileMemoryAddressStart;
  uint32 declaredFileSize = *(uint32*)(b + 2);
  void* startingAddressOfPixelsInFile = b + *(uint32*)(b + 10);
  uint32 width = *(uint32*)(b + 18);
  uint32 height = *(uint32*)(b + 22);

  if(declaredFileSize != fileSize)
  {
    dumpMessage("Bitmap file appears corrupt.");
    return;
  }

  newBitmap->width = width;
  newBitmap->height = height;
  newBitmap->pixelArrayStart = (uint32*)malloc(width * height * 4);

  uint32 nPixels = width * height;
  uint32* pa = newBitmap->pixelArrayStart;
  uint32* fa = (uint32*)startingAddressOfPixelsInFile;
  for(uint32 i = 0; i < nPixels; i++)
  {
    *pa = *fa;
    pa++;
    fa++;
  }

  free(wholeFileMemoryAddressStart);

  // flip the image on y axis

  uint32* nicePixelsStart = (uint32*)malloc(nPixels * 4);
  uint32* nastyPixel = newBitmap->pixelArrayStart;

  for(uint32 i = 0; i < nPixels; i++)
  {
    uint32 rowInNastyPixels = i / width;

    *(nicePixelsStart + (i % width) + ((height - rowInNastyPixels - 1) * width)) = *nastyPixel;

    nastyPixel++;
  }

  free(newBitmap->pixelArrayStart);
  newBitmap->pixelArrayStart = nicePixelsStart;

  generateBitmapTransparencyMap(newBitmap);

  storeItem(allBitmaps, newBitmap, name);
}

Bitmap* retrieveBitmap(const char* name)
{
  Bitmap* retrievedBitmap = (Bitmap*)retrieveItem(allBitmaps, name);
  
  if(!retrievedBitmap)
  {
    dumpMessage("Could not retrieve bitmap.");
    return 0;
  }
  
  return retrievedBitmap;
}

void generateSpritesheet(Bitmap* bitmap, const char* name, uint16 frameWidth)
{
  uint32 framesPerRow = 3;
  uint32 rowsPerSpritesheet = 4;

  if(frameWidth * framesPerRow != bitmap->width || frameWidth * rowsPerSpritesheet != bitmap->height)
  {
    dumpMessage("Bitmap validation failed; bitmap dimensions do not match desired frameWidth");
    return;
  }

  dumpMessage("Creating spritesheet from bitmap...");

  Spritesheet* newSpritesheet = (Spritesheet*)malloc(sizeof(Spritesheet));

  for(uint32 j = 0; j < rowsPerSpritesheet; j++)
  {
    for(uint32 i = 0; i < framesPerRow; i++)
    {
      newSpritesheet->allFrames[j][i] = generateSubBitmap(bitmap, i * frameWidth, j * frameWidth, frameWidth, frameWidth);
    }
  }

  storeItem(allSpritesheets, newSpritesheet, name);
}

Spritesheet* retrieveSpritesheet(const char* name)
{
  Spritesheet* retrievedSpritesheet = (Spritesheet*)retrieveItem(allSpritesheets, name);
  
  if(!retrievedSpritesheet)
  {
    dumpMessage("Could not retrieve spritesheet.");
    return 0;
  }
  
  return retrievedSpritesheet;
}

void generateFontSet(Bitmap* bitmap, const char* name, uint16 headHeight, uint16 tailHeight, ColorRGBA* maskColor)
{
  if( ((bitmap->width / NUMBER_OF_CHARACTERS_PER_FONT) * NUMBER_OF_CHARACTERS_PER_FONT) != bitmap->width)
  {
    dumpMessage("Bitmap validation failed; bitmap width is not evenly divisible into font character number.");
    return;
  }

  dumpMessage("Creating fontset from bitmap...");

  uint16 characterWidth = bitmap->width / NUMBER_OF_CHARACTERS_PER_FONT;
  uint16 characterHeight = bitmap->height;

  FontSet* newFontSet = (FontSet*)malloc(sizeof(FontSet));

  newFontSet->headHeight = headHeight;
  newFontSet->tailHeight = tailHeight;

  for(uint16 i = 0; i < NUMBER_OF_CHARACTERS_PER_FONT; i++)
  {
    Bitmap* characterBitmap = generateSubBitmap(bitmap, i * characterWidth, 0, characterWidth, characterHeight);
    
    if(maskColor != 0)
    {
      applyColorMaskToBitmap(characterBitmap, maskColor);
    }

    newFontSet->characters[i] = characterBitmap;
  }

  storeItem(allFontSets, newFontSet, name);
}

FontSet* retrieveFontSet(const char* name)
{
  FontSet* retrievedFontSet = (FontSet*)retrieveItem(allFontSets, name);
  
  if(!retrievedFontSet)
  {
    dumpMessage("Could not retrieve fontset.");
    return 0;
  }
  
  return retrievedFontSet;
}

void preloadAssets()
{
  allBitmaps = createQuickMap();
  allSpritesheets = createQuickMap();
  allFontSets = createQuickMap();
  
  preloadBitmap("../assets/rin.bmp", "RIN_BITMAP");
  generateSpritesheet(retrieveBitmap("RIN_BITMAP"), "RIN_SPRITESHEET", 32);

  ColorRGBA green;
  green.r = 50;
  green.g = 220;
  green.b = 50;
  green.a = 255;

  ColorRGBA red;
  red.r = 255;
  red.g = 50;
  red.b = 50;
  red.a = 255;

  preloadBitmap("../assets/monogram-12.bmp", "MONOGRAM_FONT_SMALL_BITMAP");
  generateFontSet(retrieveBitmap("MONOGRAM_FONT_SMALL_BITMAP"), "MONOGRAM_FONT_SMALL_BLACK", 2, 2);
  generateFontSet(retrieveBitmap("MONOGRAM_FONT_SMALL_BITMAP"), "MONOGRAM_FONT_SMALL_GREEN", 2, 2, &green);
  generateFontSet(retrieveBitmap("MONOGRAM_FONT_SMALL_BITMAP"), "MONOGRAM_FONT_SMALL_RED", 2, 2, &red);

  preloadBitmap("../assets/coffee.bmp", "COFFEE_BITMAP");
  preloadBitmap("../assets/airport.bmp", "AIRPORT_BITMAP");
  preloadBitmap("../assets/forest.bmp", "FOREST_BITMAP");
  preloadBitmap("../assets/ocean.bmp", "OCEAN_BITMAP");
  preloadBitmap("../assets/test.bmp", "TEST_BITMAP");
}