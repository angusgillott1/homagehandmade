#include "file.h"

uint32 getFileSize(const char* filename)
{
  HANDLE fileHandle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING,0,0);

  if(fileHandle == INVALID_HANDLE_VALUE)
  {
    dumpMessage("Failed to get file size.");
    return 0;
  }

  LARGE_INTEGER fileSize;
  GetFileSizeEx(fileHandle, &fileSize);

  CloseHandle(fileHandle);

  return (uint32)fileSize.QuadPart;
}

void loadEntireFileIntoMemory(const char* filename, uint32 fileSize, void* memoryAddress)
{
  HANDLE fileHandle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);

  DWORD bytesRead;
  if (ReadFile(fileHandle, (LPVOID)memoryAddress, fileSize, &bytesRead, 0) && (fileSize == bytesRead))
  {
    // success
    dumpMessage("Loaded file.");
  }
  else
  {
    dumpMessage("Failed to load file.");
  }

  CloseHandle(fileHandle);
}