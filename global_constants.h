#pragma once

#include "typedefs.h"

static const int32 WINDOW_WIDTH = 960;
static const int32 WINDOW_HEIGHT = 768;

static const int32 ASCII_CHAR_WIDTH = 5;
static const int32 ASCII_CHAR_HEIGHT = 7;
static const int32 ASCII_CHAR_GAP = 1;

static const int32 ASCII_CHAR_PRINT_WIDTH = ASCII_CHAR_WIDTH + ASCII_CHAR_GAP;
static const int32 ASCII_CHAR_PRINT_HEIGHT = ASCII_CHAR_HEIGHT + ASCII_CHAR_GAP;

static const int32 MAX_ASCII_BITMAP_WIDTH = 256;
static const int32 MAX_ASCII_BITMAP_HEIGHT = 256;

static const uint16 NUMBER_OF_CHARACTERS_PER_FONT = 128;