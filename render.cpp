#include "render.h"

BITMAPINFO bmpInfo = {}; // These are set up by setupBlitBuffer()
uint32* blitBuffer = 0;

boolean_int isRegionTotallyOffscreen(int32 left, int32 top, int32 right, int32 bottom)
{
  return (int32)(top >= WINDOW_HEIGHT || bottom < 0 || left >= WINDOW_WIDTH || right < 0);
}

void forceIntoLegalBounds(int32 &left, int32 &top, int32 &right, int32 &bottom)
{
  if(left < 0)
    left = 0;

  if(top < 0)
    top = 0;

  if(bottom >= WINDOW_HEIGHT)
    bottom = WINDOW_HEIGHT - 1;

  if(right >= WINDOW_WIDTH)
    right = WINDOW_WIDTH - 1;
}

uint32 colorToUint32(ColorRGBA* color)
{
  return ((color->a << 24) | (color->r << 16) | (color->g << 8) | color->b);
}

void setPixel(int32 x, int32 y, ColorRGBA* color)
{
  int32 linearPosition = ((WINDOW_WIDTH * y) + x);
  if (linearPosition < 0 || linearPosition >= WINDOW_WIDTH * WINDOW_HEIGHT)
    return;

  uint32* pixel = blitBuffer;

  pixel += linearPosition;
  *pixel = colorToUint32(color);
}

void clearBlitBuffer()
{
  memset(blitBuffer, 0, WINDOW_WIDTH * WINDOW_HEIGHT * 4);
}

void drawStraightLine(int32 left, int32 top, int32 length, ColorRGBA* color, boolean_int isVertical)
{
  int32 right = left;
  int32 bottom = top;

  if(isVertical)
    bottom += length - 1;
  else
    right += length - 1;
  
  if(isRegionTotallyOffscreen(left, top, right, bottom))
    return;

  forceIntoLegalBounds(left, top, right, bottom);
  
  uint32* writePixel = blitBuffer + left + (top * WINDOW_WIDTH);

  int32 runLength = 0;
  if(isVertical)
    runLength = bottom - top + 1;
  else
    runLength = right - left + 1;

  for(int32 i = 0; i < runLength; i++)
  {
    *writePixel = colorToUint32(color);

    if(isVertical)
      writePixel += WINDOW_WIDTH;
    else
      writePixel++;
  }
}

void drawRectangle(int32 left, int32 top, int32 width, int32 height, ColorRGBA* color, boolean_int isOutline)
{
  int32 right = left + width - 1;
  int32 bottom = top + height - 1;
  
  if(isRegionTotallyOffscreen(left, top, right, bottom))
    return;

  forceIntoLegalBounds(left, top, right, bottom);

  width = right - left + 1;
  height = bottom - top + 1;

  if(!isOutline)
  {
    for(int32 y = top; y <= bottom; y++)
    {
      drawStraightLine(left, y, width, color);
    }
  }
  else
  {
    drawStraightLine(left, top, width, color);
    drawStraightLine(left, top, height, color, TRUE);
    drawStraightLine(left, top + height - 1, width, color);
    drawStraightLine(left + width - 1, top, height, color, TRUE);
  }
}

void renderBitmap(Bitmap* bitmap, int32 drawX, int32 drawY, Bitmap* offscreenBitmap)
{
  uint32* renderDestination = blitBuffer;
  int32 renderDestinationWidth = WINDOW_WIDTH;
  int32 renderDestinationHeight = WINDOW_HEIGHT;
  
  if(offscreenBitmap)
  {
    renderDestination = offscreenBitmap->pixelArrayStart;
    renderDestinationWidth = offscreenBitmap->width;
    renderDestinationHeight = offscreenBitmap->height;
  }
  
  int32 left = drawX;
  int32 right = left + bitmap->width - 1;
  int32 top = drawY;
  int32 bottom = top + bitmap->height - 1;

  if(isRegionTotallyOffscreen(left, top, right, bottom)) // if bitmap is totally out of bounds
    return; // nothing to be done

  Bitmap* croppedBitmap = 0;  // we will generate a cropped bitmap if part of bitmap is out of bounds
  if(left < 0 || right >= renderDestinationWidth || top < 0 || bottom >= renderDestinationHeight)
  {
    int32 croppedWidth = bitmap->width;
    int32 croppedHeight = bitmap->height;
    int32 croppedOffsetX = 0;
    int32 croppedOffsetY = 0;

    if(left < 0)
    {
      croppedWidth -= (0 - left);
      croppedOffsetX += (0 - left);
      drawX = 0;
    }
    if(right >= renderDestinationWidth)
    {
      croppedWidth -= (right - renderDestinationWidth + 1);
    }

    if(top < 0)
    {
      croppedHeight -= (0 - top);
      croppedOffsetY += (0 - top);
      drawY = 0;
    }
    if(bottom >= renderDestinationHeight)
    {
      croppedHeight -= (bottom - renderDestinationHeight + 1);
    }

    if(croppedHeight > 0 && croppedWidth > 0)
    {
      croppedBitmap = generateSubBitmap(bitmap, croppedOffsetX, croppedOffsetY, croppedWidth, croppedHeight);
    }
  }

  Bitmap* bitmapToReallyRender = bitmap;
  if(croppedBitmap != 0)
  {
    bitmapToReallyRender = croppedBitmap; // use cropped bitmap if one was needed
  }

  uint32* srcPixel = bitmapToReallyRender->pixelArrayStart;
  srcPixel = bitmapToReallyRender->pixelArrayStart;
  int32 nX = bitmapToReallyRender->width;
  int32 nY = bitmapToReallyRender->height;

  int32 i = 0;
  int32 k = 0;
  int32 remainingPixelsInRun = bitmapToReallyRender->transparencyMap[k];
  for (int32 y = 0; y < nY; y++)
  {
    // OPTIMISED ALGORITHM, WITH TRANSPARENCY (150/50/5)
    int32 remainingPixelsInRow = nX;
    while (remainingPixelsInRow != 0)
    {
      int32 nPixelsToCopy = 0;

      if (remainingPixelsInRun <= remainingPixelsInRow)
      {
        nPixelsToCopy = remainingPixelsInRun;
      }
      else
      {
        nPixelsToCopy = remainingPixelsInRow;
      }

      if (k % 2 == 1)
      {
        int32 writeOffset = (i % nX) + drawX + (((i / nX) + drawY) * renderDestinationWidth);
        memcpy(renderDestination + writeOffset, srcPixel + i, 4 * nPixelsToCopy);
      }

      i += nPixelsToCopy;
      remainingPixelsInRow -= nPixelsToCopy;
      remainingPixelsInRun -= nPixelsToCopy;

      if (remainingPixelsInRun == 0)
      {
        k++;
        if (k < nX * nY) // guard against the edge case that entire sprite is checkerboard, which would cause the k to advance one past end of data structure
        {
          remainingPixelsInRun = bitmapToReallyRender->transparencyMap[k];
        }
      }
    }

    // FAST, WITHOUT TRANSPARENCY (185/105/20)
    /*
    memcpy(renderDestination + (i % nX) + drawX + ((uint32)(((i) / nX) + drawY) * renderDestinationWidth), srcPixel + i, 4 * nX);
    i += nX;
    */
  }

  if(croppedBitmap != 0)
  {
    freeBitmap(croppedBitmap);
  }
}

void setupBlitBuffer()
{
  bmpInfo.bmiHeader.biSize = sizeof(bmpInfo.bmiHeader);
  bmpInfo.bmiHeader.biWidth = WINDOW_WIDTH;
  bmpInfo.bmiHeader.biHeight = 0 - WINDOW_HEIGHT;
  bmpInfo.bmiHeader.biPlanes = 1;
  bmpInfo.bmiHeader.biBitCount = 32;
  bmpInfo.bmiHeader.biCompression = BI_RGB;

  blitBuffer = (uint32*)malloc(WINDOW_WIDTH * WINDOW_HEIGHT * 4);
}

void blitBufferToWindow(HWND window)
{
  HDC deviceContext = GetDC(window);
  StretchDIBits(deviceContext, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, blitBuffer, &bmpInfo, DIB_RGB_COLORS,SRCCOPY);
  ReleaseDC(window, deviceContext);
}

void drawTextAtAbsolutePosition(const char* str, int32 x, int32 y, int32 characterSpacing, ColorRGBA* color, const char* fontset, Bitmap* offscreenBitmap)
{
  FontSet* fontSet = retrieveFontSet(fontset);
  if(color != 0)
  {
    // TODO: account for custom color masking
  }

  int32 drawX = x;
  int32 drawY = y;

  uint16 charIndex = 0;
  for(char c = str[charIndex]; str[charIndex] != '\0'; charIndex++)
  {
    Bitmap* charBitmap = fontSet->characters[str[charIndex]];
    renderBitmap(charBitmap, drawX, drawY, offscreenBitmap);
    drawX += charBitmap->width + characterSpacing;
  }
}

Bitmap* generateAsciiImage(Bitmap* bitmap)
{
  FontSet* defaultFontSet = retrieveFontSet("MONOGRAM_FONT_SMALL_GREEN");

  char* asciiPixels = (char*)malloc(MAX_ASCII_BITMAP_HEIGHT * MAX_ASCII_BITMAP_WIDTH);  // layout [y][x] (in memory, full rows sequentially)

  int32 bitmapAsciiWidth = bitmap->width / ASCII_CHAR_PRINT_WIDTH;
  int32 bitmapAsciiHeight = bitmap->height / ASCII_CHAR_PRINT_HEIGHT;

  for(int32 y = 0; y < bitmapAsciiHeight; y++)
  {
    for(int32 x = 0; x < bitmapAsciiWidth; x++)
    {
      Bitmap* tileBitmap = generateSubBitmap(bitmap, (x * ASCII_CHAR_PRINT_WIDTH), (y * ASCII_CHAR_PRINT_HEIGHT), ASCII_CHAR_WIDTH, ASCII_CHAR_HEIGHT);

      int32 bestMatchIndex = -1;
      int32 bestMatchValue = -MAX_POSSIBLE_INT_32;

      for(int32 z = 0; z < getNumberOfCharactersPerFont(); z++)
      {
        Bitmap* characterBitmap = defaultFontSet->characters[z];
        int32 appropriateness = 0;

        for(int32 pixelOffset = 0; pixelOffset < ASCII_CHAR_HEIGHT * ASCII_CHAR_WIDTH; pixelOffset++)
        {
          //(color->a << 24) | (color->r << 16) | (color->g << 8) | color->b
          int32 bmpPixelIntensity =
          (
            (int32)(((255 << 16) & *(tileBitmap->pixelArrayStart + pixelOffset)) >> 16) +  // r
            (int32)(((255 << 8) & *(tileBitmap->pixelArrayStart + pixelOffset)) >> 8) +  // g
            (int32)(((255 << 0) & *(tileBitmap->pixelArrayStart + pixelOffset)) >> 0)  // b
          ) / 3;

          int32 charPixelIntensity =
          (
            (int32)(((255 << 16) & *(characterBitmap->pixelArrayStart + pixelOffset)) >> 16) +  // r
            (int32)(((255 << 8) & *(characterBitmap->pixelArrayStart + pixelOffset)) >> 8) +  // g
            (int32)(((255 << 0) & *(characterBitmap->pixelArrayStart + pixelOffset)) >> 0)  // b
          ) / 3;

          appropriateness += abs(charPixelIntensity - bmpPixelIntensity);
        }

        if(appropriateness > bestMatchValue)
        {
          bestMatchValue = appropriateness;
          bestMatchIndex = z;
        }
      }

      *(asciiPixels + (y * bitmapAsciiWidth + x)) = (char)bestMatchIndex;
      freeBitmap(tileBitmap);
    }
  }

  char line[MAX_ASCII_BITMAP_WIDTH + 1];  // The 1 is for the '\0' character.
  line[bitmapAsciiWidth] = '\0';

  Bitmap* asciiBitmap = (Bitmap*)malloc(sizeof(Bitmap)); 
  asciiBitmap->width = bitmap->width;
  asciiBitmap->height = bitmap->height;
  asciiBitmap->pixelArrayStart = (uint32*)malloc(asciiBitmap->width * asciiBitmap->height * 4);
  memset(asciiBitmap->pixelArrayStart, 0, asciiBitmap->width * asciiBitmap->height * 4);

  for(int32 y = 0; y < bitmapAsciiHeight; y++)
  {
    memcpy(line, (asciiPixels + y * bitmapAsciiWidth), bitmapAsciiWidth);
    drawTextAtAbsolutePosition(line, 0, y * ASCII_CHAR_PRINT_HEIGHT, ASCII_CHAR_GAP, 0, "MONOGRAM_FONT_SMALL_GREEN", asciiBitmap);
  }

  generateBitmapTransparencyMap(asciiBitmap);

  free(asciiPixels);

  return asciiBitmap;
}