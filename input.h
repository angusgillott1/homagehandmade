#pragma once

#include <windows.h>

#include "typedefs.h"

void scanKeys();

boolean_int* getKeyDownArray();
boolean_int* getKeyWasDownArray();