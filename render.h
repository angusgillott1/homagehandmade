#pragma once

#include <windows.h>

#include "typedefs.h"
#include "global_constants.h"
#include "file.h"
#include "assets.h"
#include "utils.h"

boolean_int isRegionTotallyOffscreen(int32 left, int32 top, int32 right, int32 bottom);
void forceIntoLegalBounds(int32 &left, int32 &top, int32 &right, int32 &bottom);

uint32 colorToUint32(ColorRGBA* color);

void setPixel(int32 x, int32 y, ColorRGBA* color);

void clearBlitBuffer();

void drawStraightLine(int32 left, int32 top, int32 length, ColorRGBA* color, boolean_int isVertical = false);
void drawRectangle(int32 left, int32 top, int32 width, int32 height, ColorRGBA* color, boolean_int isOutline = false);

void renderBitmap(Bitmap* bitmap, int32 drawX, int32 drawY, Bitmap* offscreenBitmap = 0); // Will render to offscreenBitmap if specified, otherwise will render to screen.

void setupBlitBuffer();
void blitBufferToWindow(HWND window);

void drawTextAtAbsolutePosition(const char* str, int32 x, int32 y, int32 characterSpacing = 1, ColorRGBA* color = 0, const char* fontset = "MONOGRAM_FONT_SMALL_GREEN", Bitmap* offscreenBitmap = 0);

Bitmap* generateAsciiImage(Bitmap* bitmap);