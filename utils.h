#pragma once

#include <windows.h>

#include "typedefs.h"
#include "global_constants.h"

//------------------
// Strings
//------------------

// Fills a string of length nCharacters with character c and terminates string (nCharacters includes null terminator)
void fillString(char* strTo, char c, int32 nCharacters);

// Copies a string character array into another (skipping null terminator in From string), and returns a value indicating the next index of the To string (or -1 if the copy reached end of string)
int32 rawStringCopy(char* strTo, const char* strFrom, int32 strToIndex, int32 strFromIndex, int32 maxCharsToCopy);

// Returns 1 if two strings are the same, 0 if otherwise
boolean_int isStringMatch(const char* str1, const char* str2);


//------------------
// Debug output
//------------------

void varDump(const char* var);
void varDump(float var);

void dumpMessage(const char* msg);


//------------------
// Quickmap implementation
//------------------

//------------------
//
// Notes:
// 
// - Keys of quickmap must be non-empty strings of only capital letters and the underscore _ character, and no more than 127 characters long.
//    e.g. EXAMPLE_ITEM_KEY
// 
//------------------

const int32 N_QUICKMAP_BUCKETS = 13 * 13;
const int32 MAX_KEY_LENGTH = 128;

struct QuickMapNode // A QuickMapNode is the first node of a linked list
{
  void* item;
  QuickMapNode* nextNode;
  char key[MAX_KEY_LENGTH];
};

struct QuickMap
{
  QuickMapNode* buckets[N_QUICKMAP_BUCKETS];  // each bucket contains the first node of a linked list (or NULL)
};

QuickMapNode* createQuickMapNode(void* item, const char* key);
QuickMap* createQuickMap();

int32 quickHash(const char* str);
void pushToMapBucket(QuickMapNode* firstNode, void* item, const char* key);  // firstNode must not be 0

void* retrieveItem(QuickMap* quickMap, const char* key);
void storeItem(QuickMap* quickMap, void* item, const char* key);

