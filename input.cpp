#include "input.h"

BYTE rawKeys[256];  // used to request keyboard state from windows
boolean_int keyDown[256] = {0};
boolean_int keyWasDown[256] = {0};

void scanKeys()
{
  GetKeyboardState(rawKeys);
  memcpy(keyWasDown, keyDown, sizeof(boolean_int) * 256);

  for(uint32 i = 0; i < 256; i++)
  {
    boolean_int isDown = rawKeys[i] >> 7;
    keyDown[i] = isDown;
  }
}

boolean_int* getKeyDownArray()
{
  return keyDown;
}

boolean_int* getKeyWasDownArray()
{
  return keyWasDown;
}