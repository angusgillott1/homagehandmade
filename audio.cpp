#include "audio.h"

QuickMap* allSounds;  // Holds Mix_Chunk*
QuickMap* allMusics;  // Holds Mix_Music*

void preloadSound(const char* filepath, const char* name)
{
  Mix_Chunk* s = Mix_LoadWAV(filepath);
  
  if(!s)
  {
    dumpMessage("Failed to load sound.");
    dumpMessage(filepath);
  }

  storeItem(allSounds, s, name); 
}

Mix_Chunk* retrieveSound(const char* name)
{
  Mix_Chunk* retrievedSound = (Mix_Chunk*)retrieveItem(allSounds, name);
  
  if(!retrievedSound)
  {
    dumpMessage("Could not retrieve sound.");
    return 0;
  }
  
  return retrievedSound;
}

void playSound(Mix_Chunk* sound)
{
  Mix_PlayChannel(-1, sound, 0);
}

void preloadMusic(const char* filepath, const char* name)
{
  Mix_Music* m = Mix_LoadMUS(filepath);

  if(!m)
  {
    dumpMessage("Failed to load music.");
    dumpMessage(filepath);
  }

  storeItem(allMusics, m, name); 
}

Mix_Music* retrieveMusic(const char* name)
{
  Mix_Music* retrievedMusic = (Mix_Music*)retrieveItem(allMusics, name);
  
  if(!retrievedMusic)
  {
    dumpMessage("Could not retrieve music.");
    return 0;
  }
  
  return retrievedMusic;
}

void playMusic(Mix_Music* music)
{
  Mix_PlayMusic(music, -1);
}

void preloadSoundAssets()
{
  preloadSound("../assets/wolf-growl.wav", "WOLF_GROWL");

  preloadMusic("../assets/bit-quest.wav", "BIT_QUEST");
}

void setupAudio()
{
  SDL_Init(SDL_INIT_AUDIO);
  
  if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
  {
    dumpMessage("SDL_mixer could not initialize.");
  }

  allSounds = createQuickMap();
  allMusics = createQuickMap();
}

//Mix_FreeChunk(gWolf);
//Mix_FreeChunk(gMusic);