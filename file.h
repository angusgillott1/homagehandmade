#pragma once

#include <windows.h>

#include "typedefs.h"
#include "utils.h"

uint32 getFileSize(const char* filename);
void loadEntireFileIntoMemory(const char* filename, uint32 fileSize, void* memoryAddress);