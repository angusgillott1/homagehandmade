#pragma once

#include <windows.h>

#include "file.h"
#include "typedefs.h"
#include "global_constants.h"
#include "utils.h"

struct ColorRGBA
{
  byte r = 0;
  byte g = 0;
  byte b = 0;
  byte a = 0;
};

struct Bitmap
{
  uint16 width = 0;
  uint16 height = 0;

  uint32* pixelArrayStart = 0;
  uint32* transparencyMap = 0; // an array specifying N TRANSPARENT BITS, N OPAQUE BITS, N TRANSPARENT BITS, N OPAQUE BITS...
};

struct Spritesheet
{
  Bitmap* allFrames[4][3] = {0};
};

struct FontSet
{
  Bitmap* characters[NUMBER_OF_CHARACTERS_PER_FONT] = {0};
  uint16 headHeight = 0;
  uint16 tailHeight = 0;
};

int32 getNumberOfCharactersPerFont();

void freeBitmap(Bitmap* bitmap);

Bitmap* generateSubBitmap(Bitmap* bitmap, uint32 sampleOffsetX, uint32 sampleOffsetY, uint32 sampleWidth, uint32 sampleHeight);
void generateBitmapTransparencyMap(Bitmap* bitmap);

void applyColorMaskToBitmap(Bitmap* bitmap, ColorRGBA* maskColor);

void preloadBitmap(const char* filepath, const char* name);
Bitmap* retrieveBitmap(const char* name);

void generateSpritesheet(Bitmap* bitmap, const char* name, uint16 frameWidth);
Spritesheet* retrieveSpritesheet(const char* name);

void generateFontSet(Bitmap* bitmap, const char* name, uint16 headHeight, uint16 tailHeight, ColorRGBA* maskColor = 0);
FontSet* retrieveFontSet(const char* name);

void preloadAssets();