#pragma once

#pragma warning(disable: 26812)
#pragma warning(disable: 6011)
#pragma warning(disable: 6001)
#pragma warning(disable: 6387)
#pragma warning(disable: 6386)
#pragma warning(disable: 4244)
#pragma warning(disable: 28182)
#pragma warning(disable: 6031)

#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t uint16;
typedef int16_t int16;
typedef uint32_t uint32;
typedef int32_t int32;
typedef uint32_t boolean_int;
typedef uint64_t uint64;
typedef int64_t int64;

static const int32 MAX_POSSIBLE_INT_32 = 2147483647;
static const uint32 MAX_POSSIBLE_UINT_32 = 4294967295;