#include <windows.h>
#include <winuser.h>

#include "typedefs.h"
#include "global_constants.h"
#include "input.h"
#include "render.h"
#include "file.h"
#include "assets.h"
#include "audio.h"
#include "utils.h"
#include "tests.h"

boolean_int shouldQuit = 0;

uint32 globalTick = 0;
uint64 performanceCounterFrequency;
uint64 initialPerformanceCounter;

boolean_int windowDragPressedDown = 0;
int32 windowDragPressedX = 0;
int32 windowDragPressedY = 0;

float getWallClockInSeconds()
{
  LARGE_INTEGER wallCounter;
  QueryPerformanceCounter(&wallCounter);

  return (wallCounter.QuadPart - initialPerformanceCounter) / (float)performanceCounterFrequency;
}

float timeDifferenceInSeconds(float start, float end)
{
  if(end < start)
  {
    return start - end;
  }
  return end - start;
}

void adjustWindowLocation(HWND window)
{
  if(!windowDragPressedDown)
  {
    return;
  }

  POINT cursorPosition;
  GetCursorPos(&cursorPosition);

  MoveWindow(window, cursorPosition.x - windowDragPressedX, cursorPosition.y - windowDragPressedY, WINDOW_WIDTH, WINDOW_HEIGHT, 1);
}

LRESULT CALLBACK MainWindowCallback(
  _In_ HWND   window,
  _In_ UINT   message,
  _In_ WPARAM wParam,
  _In_ LPARAM lParam
)
{
  LRESULT result = 0;

  switch(message)
  {
    case WM_DESTROY:
    {
      shouldQuit = 1;
    } break;

    case WM_CLOSE:
    {
      shouldQuit = 1;
    } break;

    case WM_ACTIVATEAPP:
    {

    } break;

    case WM_LBUTTONDOWN:
    {
      windowDragPressedDown = 1;
      windowDragPressedX = lParam & 0xFFFF;
      windowDragPressedY = (lParam & 0xFFFF0000) >> 16;
    } break;

    case WM_LBUTTONUP:
    {
      windowDragPressedDown = 0;
    } break;

    case WM_PAINT:
    {
      PAINTSTRUCT paint;
      BeginPaint(window, &paint);
      blitBufferToWindow(window);
      EndPaint(window, &paint);
    } break;

    default:
    {
      result = DefWindowProcA(window, message, wParam, lParam);
    } break;
  }

  return result;
}

int WINAPI wWinMain(
  _In_ HINSTANCE instance,
  _In_opt_ HINSTANCE prevInstance,
  _In_ LPWSTR commandLine,
  _In_ int showCode)
{ 
  WNDCLASSA windowClass = {};

  uint32 actualMonitorWidth = GetSystemMetrics(SM_CXSCREEN);
  uint32 actualMonitorHeight = GetSystemMetrics(SM_CYSCREEN);

  uint32 windowPositionX = (actualMonitorWidth - WINDOW_WIDTH) / 4;
  uint32 windowPositionY = (actualMonitorHeight - WINDOW_HEIGHT) / 2;

  if(windowPositionX < 0) windowPositionX = 0;
  if(windowPositionY < 0) windowPositionY = 0;

  windowClass.style = CS_OWNDC;
  windowClass.lpfnWndProc = MainWindowCallback;
  windowClass.hInstance = instance;
  //windowClass.hIcon;
  windowClass.lpszClassName = "HomageWindowClass";

  RegisterClassA(&windowClass);
  HWND window = CreateWindowA(
    windowClass.lpszClassName,
    "Ascii",
    WS_VISIBLE | WS_POPUP,
    windowPositionX,
    windowPositionY,
    WINDOW_WIDTH,
    WINDOW_HEIGHT,
    0,
    0,
    windowClass.hInstance,
    0);

  if(!window)
  {
    dumpMessage("Window was not successfully created.");
    return 1;
  }

  //------------
  // Setup
  //------------
  
  setupAudio();

  preloadSoundAssets();
  preloadAssets();

  setupForTesting();
  //runInitialTestSuite();

  setupBlitBuffer();

  //-------------------
  // Timer setup
  //-------------------

  boolean_int sleepIsGranular = (timeBeginPeriod(1) == TIMERR_NOERROR);
  uint32 monitorRefreshRate = 60;
  uint32 gameRefreshRate = 60;
  float secondsPerFrame = 1.0f / (float)gameRefreshRate;

  LARGE_INTEGER performanceCounterFrequencyQueryResult;
  QueryPerformanceFrequency(&performanceCounterFrequencyQueryResult);
  performanceCounterFrequency = performanceCounterFrequencyQueryResult.QuadPart;

  LARGE_INTEGER performanceCounter;
  QueryPerformanceCounter(&performanceCounter);

  initialPerformanceCounter = performanceCounter.QuadPart;

  float lastTimer = getWallClockInSeconds();

  uint64 lastCycleCount = __rdtsc();

  //-------------
  // Main loop
  //-------------

  while(!shouldQuit)
  {
    globalTick++;

    MSG message;
    while(PeekMessage(&message, 0, 0, 0, PM_REMOVE))
    {
      if(message.message == WM_QUIT)
      {
        shouldQuit = 1;
      }

      TranslateMessage(&message);
      DispatchMessageA(&message);
    }

    adjustWindowLocation(window);

    scanKeys();

    boolean_int* keyDown = getKeyDownArray();

    if(keyDown['Q'] || keyDown[VK_ESCAPE] || (keyDown[VK_MENU] && keyDown[VK_F4]))
      shouldQuit = 1;

    handleDebugInput();

    // render    
    clearBlitBuffer();

    testRendering();


    blitBufferToWindow(window);
    // done renders

    float endTimer = getWallClockInSeconds();
    uint64 endCycleCount = __rdtsc();

    float secondsElapsedToComputeFrame = timeDifferenceInSeconds(lastTimer, endTimer);
    uint32 potentialFramesPerSecond = (int)(1.0 / secondsElapsedToComputeFrame);  // The frame rate we could hit, given time to compute frame

    uint64 cycleCountsElapsed = endCycleCount - lastCycleCount;

    float secondsElapsedOnCurrentFrame = secondsElapsedToComputeFrame;
      
    if(sleepIsGranular)
    {
      int32 timeToSleep = (int)(1000 * (secondsPerFrame - secondsElapsedOnCurrentFrame)) - 2;
      if(timeToSleep > 0)
      {
        Sleep(timeToSleep);
      }
    }
    while (secondsElapsedOnCurrentFrame < secondsPerFrame)
    {
      endTimer = getWallClockInSeconds();
      secondsElapsedOnCurrentFrame = timeDifferenceInSeconds(lastTimer, endTimer);
    }

    endCycleCount = __rdtsc();

    endTimer = getWallClockInSeconds();
    secondsElapsedOnCurrentFrame = timeDifferenceInSeconds(lastTimer, endTimer);

    lastTimer = endTimer;
    lastCycleCount = endCycleCount;
  }

  return 0;
}
